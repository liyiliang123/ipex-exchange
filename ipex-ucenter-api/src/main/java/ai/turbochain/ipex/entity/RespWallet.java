package ai.turbochain.ipex.entity;

import lombok.Data;

/**
 * @author 未央
 * @create 2019-12-16 16:21
 */
@Data
public class RespWallet {

    private RespCurrencyWallet wallet;

    private Long otcCoin_id;

}
