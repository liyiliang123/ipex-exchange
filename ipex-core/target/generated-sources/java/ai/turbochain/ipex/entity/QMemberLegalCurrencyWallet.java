package ai.turbochain.ipex.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMemberLegalCurrencyWallet is a Querydsl query type for MemberLegalCurrencyWallet
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMemberLegalCurrencyWallet extends EntityPathBase<MemberLegalCurrencyWallet> {

    private static final long serialVersionUID = 683778349L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMemberLegalCurrencyWallet memberLegalCurrencyWallet = new QMemberLegalCurrencyWallet("memberLegalCurrencyWallet");

    public final NumberPath<java.math.BigDecimal> balance = createNumber("balance", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> frozenBalance = createNumber("frozenBalance", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> isLock = createEnum("isLock", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final NumberPath<Long> memberId = createNumber("memberId", Long.class);

    public final QOtcCoin otcCoin;

    public final NumberPath<java.math.BigDecimal> toReleased = createNumber("toReleased", java.math.BigDecimal.class);

    public final NumberPath<Integer> version = createNumber("version", Integer.class);

    public QMemberLegalCurrencyWallet(String variable) {
        this(MemberLegalCurrencyWallet.class, forVariable(variable), INITS);
    }

    public QMemberLegalCurrencyWallet(Path<? extends MemberLegalCurrencyWallet> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMemberLegalCurrencyWallet(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMemberLegalCurrencyWallet(PathMetadata metadata, PathInits inits) {
        this(MemberLegalCurrencyWallet.class, metadata, inits);
    }

    public QMemberLegalCurrencyWallet(Class<? extends MemberLegalCurrencyWallet> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.otcCoin = inits.isInitialized("otcCoin") ? new QOtcCoin(forProperty("otcCoin")) : null;
    }

}

